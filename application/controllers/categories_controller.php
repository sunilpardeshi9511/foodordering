<?php

class categories_controller extends CI_Controller{
	
	public function __construct() { 
		 parent::__construct();
		 $this->load->library('session');
		 if( ! $this->session->userdata('id') )
		{
			return redirect('Registration_controller/login2');
		}
    }
	
	function index()
	{	
		$this->load->view('index');
	}
	
	function edit(){
		
		$this->load->model('categories_model');
		$EmployeeId = $this->input->post("userId");
		$this->db->select("*");
		$this->db->where("id",$EmployeeId);
		$this->db->from('categories');
		$qryEmployeeDetails = $this->db->get();
		$rstEmployeeDetails = $qryEmployeeDetails->result();
		if(!empty($rstEmployeeDetails)):
            foreach($rstEmployeeDetails as $row):
				echo $row->id."--x--".$row->category_name."--x--".base_url()."upload/".$row->image."--x--".$row->status."--x--".$row->created_at."--x--".$row->updated_at;			
			endforeach;
        endif;
		
	}
	
	function deletedata(){
		
			$this->load->model('categories_model');
			$userId = $this->input->post("userId");
			$this->categories_model->deleteUser($userId);
			$this->load->view('categories');
			//redirect('categories_controller/categories');
			
	}
	
	function update(){
		
		$config = ['upload_path' => './upload','allowed_types' => 'gif|jpg|png|jpeg'];
		$this->load->library('upload', $config);
		$this->load->model('categories_model');
		
		$formArray = array();
		$userId = $this->input->post("cId");
		$formArray['category_name']=$this->input->post("cname");
		$formArray['updated_at'] = date('Y-m-d H:i:s');
		if($_FILES["userfile"]['name'] !="")
				{
					$this->upload->set_filename("user_profile_".time()."jpg");
					$this->upload->do_upload();
					$uplodadata = $this->upload->data();
					$imagefilename = $uplodadata['file_name'];
					$formArray['image'] = $imagefilename;
				}
		$this->categories_model->updateUser($userId,$formArray);
		redirect('categories_controller/categories');
	}
	
	
	
	function categories(){
		
		$this->load->view('index');
		$this->load->model('categories_model');
		$users = $this->categories_model->categories_list();
		$data = array();
		$data['users']= $users; 
		$this->load->view('categories',$data);
		$this->load->view('footer');
	}
	
	function addCategories(){
		
		$config = ['upload_path' => './upload','allowed_types' => 'gif|jpg|png|jpeg'];
		$this->load->library('upload', $config);
		$this->load->model('categories_model');
		$this->form_validation->set_rules('cname','Category Name','required');
		if($this->form_validation->run() && $this->upload->do_upload()){
			$formArray = array();
			$formArray['category_name']= $this->input->post('cname');
			$formArray['created_at'] = date('Y-m-d H:i:s');
			$formArray['status'] = 1;
			$data = $this->upload->data();
			$image_path = $data['file_name'];
			$formArray['image'] = $image_path;
			$this->categories_model->addCategory($formArray);
			redirect('categories_controller/categories');
		}
		else{
			
			$this->load->view('categories', compact('upload_error'));
		}
		
	}
	function editCategories($userId){
		
		$config = ['upload_path' => './upload','allowed_types' => 'gif|jpg|png|jpeg'];
		$this->load->library('upload', $config);
		
			$this->load->model('categories_model');
			$user = $this->categories_model->getUser($userId);
			$data = array();
			$data['user']= $user;
			
		$this->form_validation->set_rules('cname','Category Name','required');
		
		if($this->form_validation->run()){
			$formArray = array();
			$formArray['category_name']= $this->input->post('cname');
			$formArray['created_at'] = date('Y-m-d H:i:s');
			 
			if($_FILES["userfile"]['name'] !="")
				{
					$this->upload->set_filename("user_profile_".time()."jpg");
					$this->upload->do_upload();
					$uplodadata = $this->upload->data();
					$imagefilename = $uplodadata['file_name'];
					$formArray['image'] = $imagefilename;
				}
				$this->categories_model->updateUser($userId,$formArray);
				redirect(base_url().'index.php/categories_controller/categories');
			}
			else{
				$this->load->view('editCategories',$data,compact('upload_error'));
			}
		}
		
		
}
?>